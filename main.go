package main

import (
	"context"
	"fmt"
	"os"
	"os/exec"
	"os/signal"
	"reflect"
	"sort"
	"strings"
	"sync"
	"syscall"
	"text/tabwriter"
	"time"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/api/types/swarm"
	"github.com/docker/docker/client"
	"github.com/sirupsen/logrus"
	cli "github.com/urfave/cli/v2"
	yaml "gopkg.in/yaml.v3"
)

var (
	version            = "development"
	logger             = logrus.New()
	log                *logrus.Entry
	dockerCLI          *client.Client
	performDeploy      bool
	prune              bool
	composeFiles       []string
	jsonLog            bool
	debug              bool
	failCountThreshold int
	checkInterval      time.Duration
	statusInterval     time.Duration
	prettyReport       bool

	yes = true
	no  = false
)

type failedStatus struct {
	Err   string
	Count int
}

type statusContextType struct {
	context.Context
	mu                             *sync.RWMutex
	updating, completed, untouched map[string]struct{}
	rolledBack, rollbackStarted    map[string]string
	failed                         map[string]failedStatus
	successfulTasks                map[string]uint64
	desiredReplicas                map[string]uint64
	servicesBefore                 map[string]*swarm.Service
	serviceNames                   []string
	nodeIDs                        []string
}

func main() {
	app := &cli.App{
		UseShortOptionHandling: true,
		Name:                   "docker-stack-waiter",
		Usage:                  "Wait for a stack deploy to complete, and optionally start it first",
		UsageText:              "docker-stack-waiter [options] STACK_NAME",
		Authors: []*cli.Author{
			{
				Name:  "Sune Keller",
				Email: "absukl@almbrand.dk",
			},
			{
				Name:  "Loke Norlin Johannessen",
				Email: "ablojh@almbrand.dk",
			},
		},
		Commands: []*cli.Command{
			{
				Hidden: true,
				Name:   "test-node-filter",
				Action: func(c *cli.Context) error {
					dockerCLI, err := client.NewClientWithOpts(client.FromEnv, client.WithVersion("1.37"))
					if err != nil {
						log.WithFields(logrus.Fields{
							"error": err,
						}).Fatal("Error creating Docker client")
					}
					nodes, err := dockerCLI.NodeList(context.Background(), types.NodeListOptions{})
					if err != nil {
						return fmt.Errorf("")
					}
					taskFilters := filters.NewArgs()
					for _, node := range nodes {
						taskFilters.Add("node", node.ID)
					}

					currentTasks, err := dockerCLI.TaskList(context.Background(), types.TaskListOptions{
						Filters: taskFilters,
					})

					for _, task := range currentTasks {
						log.WithFields(logrus.Fields{
							"task": task.ID,
							"node": task.NodeID,
						}).Info("")
					}

					return nil
				},
			},
			{
				Hidden: true,
				Name:   "test-pretty-report",
				Action: func(c *cli.Context) error {
					printPrettyReport(statusContextType{
						mu:           &sync.RWMutex{},
						Context:      context.Background(),
						serviceNames: []string{"stack_service-foo", "stack_service-bar", "stack_service-bazookas"},
						updating:     map[string]struct{}{"stack_service-foo": {}, "stack_service-bazookas": {}},
						completed:    map[string]struct{}{"stack_service-foo": {}},
						untouched:    map[string]struct{}{"stack_service-bar": {}},
						failed:       map[string]failedStatus{"stack_service-bazookas": {Err: "no good", Count: 3}},
					})
					return nil
				},
			},
		},
		Version: version,
		Flags: []cli.Flag{
			&cli.StringSliceFlag{
				Name:    "file",
				Value:   &cli.StringSlice{},
				Aliases: []string{"compose-file", "c"},
				Usage:   "List of Docker Compose files",
			},
			&cli.BoolFlag{
				Name:        "deploy",
				Destination: &performDeploy,
				Usage:       "Run docker stack deploy (requires docker binary)",
			},
			&cli.BoolFlag{
				Name:        "prune",
				Destination: &prune,
				Usage:       "Prune services that are no longer referenced",
			},
			&cli.BoolFlag{
				Name:        "json",
				Destination: &jsonLog,
				Usage:       "Log in JSON format",
				EnvVars:     []string{"DSW_JSON"},
			},
			&cli.BoolFlag{
				Name:        "debug",
				Destination: &debug,
				Usage:       "Debug",
				EnvVars:     []string{"DSW_DEBUG"},
			},
			&cli.IntFlag{
				Name:        "task-failure-threshold",
				Destination: &failCountThreshold,
				Usage:       "How many times a service has to be observed with one or more failing tasks before the service is considered failed",
				EnvVars:     []string{"DSW_TASK_FAILURE_THRESHOLD"},
				Value:       5,
			},
			&cli.DurationFlag{
				Name:        "check-interval",
				Value:       250 * time.Millisecond,
				Destination: &checkInterval,
				Usage:       "How often to check the status (min. 250ms)",
				EnvVars:     []string{"DSW_CHECK_INTERVAL"},
			},
			&cli.DurationFlag{
				Name:        "status-interval",
				Value:       10 * time.Second,
				Destination: &statusInterval,
				Usage:       "How often to print the current status (min. 1s)",
				EnvVars:     []string{"DSW_STATUS_INTERVAL"},
			},
			&cli.BoolFlag{
				Name:        "pretty-report",
				Value:       true,
				Destination: &prettyReport,
				Usage:       "Print a pretty report on every status interval",
				EnvVars:     []string{"DSW_PRETTY_REPORT"},
			},
		},
		Before: func(c *cli.Context) error {
			if jsonLog {
				logger.SetFormatter(&logrus.JSONFormatter{
					FieldMap: logrus.FieldMap{
						logrus.FieldKeyTime: "@timestamp",
					},
				})
			}
			log = logger.WithField("docker-stack-waiter-version", version)

			if checkInterval < 250*time.Millisecond {
				return fmt.Errorf("check-interval (%v) must be at least 250ms", checkInterval)
			}
			if statusInterval < 1*time.Second {
				return fmt.Errorf("status-interval must be at least 1s")
			}
			if statusInterval < checkInterval {
				statusInterval = 2 * checkInterval
				log.Warnf("status-interval must be greater than check interval; setting to 2 * check-interval = %v", statusInterval)
			}
			return nil
		},
		Action: func(c *cli.Context) error {
			var err error
			if c.NArg() != 1 {
				log.WithFields(logrus.Fields{
					"error":    "Must specify namespace as first argument",
					"num_args": c.NArg(),
				}).Fatal("Error parsing arguments")
			}
			namespace := c.Args().First()
			logger.SetLevel(logrus.DebugLevel)

			composeFiles = c.StringSlice("compose-file")
			if len(composeFiles) == 0 {
				if composeFileEnv, exists := os.LookupEnv("COMPOSE_FILE"); exists {
					separator := string(os.PathSeparator)
					if composePathSeparator, exists := os.LookupEnv("COMPOSE_PATH_SEPARATOR"); exists {
						separator = composePathSeparator
					}
					composeFiles = []string(strings.Split(composeFileEnv, separator))
				} else {
					composeFiles = []string{"docker-compose.yml"}
				}
			}

			dockerCLI, err = client.NewClientWithOpts(client.FromEnv, client.WithVersion("1.37"))
			if err != nil {
				log.WithFields(logrus.Fields{
					"error": err,
				}).Fatal("Error creating Docker client")
			}

			servicesBefore := make(map[string]*swarm.Service)
			var serviceNames []string
			var composeVersion string
			// Read compose file
			for _, composeFilePath := range composeFiles {
				composeFile, err := os.Open(composeFilePath)
				if err != nil {
					log.WithFields(logrus.Fields{
						"error":        err,
						"compose-file": composeFilePath,
					}).Fatal("Error opening compose file")
				}
				decoder := yaml.NewDecoder(composeFile)
				compose := struct {
					Version  string                 `yaml:"version"`
					Services map[string]interface{} `yaml:"services"`
				}{}
				decoder.Decode(&compose)
				if len(composeVersion) == 0 {
					composeVersion = compose.Version
				} else if composeVersion != compose.Version {
					log.Fatalf("Inconsistent compose version in %s: %s; already saw version %s", composeFilePath, compose.Version, composeVersion)
				}

				// Check for and store existing service specs
				for key := range compose.Services {
					name := fmt.Sprintf("%s_%s", namespace, key)
					if _, exists := servicesBefore[name]; !exists {
						serviceNames = append(serviceNames, name)
						service := inspectService(name)
						if service != nil {
							servicesBefore[name] = service
						}
					}
				}
			}

			nodeIDs := []string{}
			nodes, err := dockerCLI.NodeList(context.Background(), types.NodeListOptions{})
			if err != nil {
				return fmt.Errorf("")
			}
			for _, node := range nodes {
				nodeIDs = append(nodeIDs, node.ID)
			}

			var (
				updating        = make(map[string]struct{})
				completed       = make(map[string]struct{})
				untouched       = make(map[string]struct{})
				rollbackStarted = make(map[string]string)
				rolledBack      = make(map[string]string)
				failed          = make(map[string]failedStatus)
				successfulTasks = make(map[string]uint64)
				desiredReplicas = make(map[string]uint64)
			)

			if performDeploy {
				log.WithFields(logrus.Fields{
					"services":        serviceNames,
					"compose-version": composeVersion,
					"compose-files":   composeFiles,
				}).Info("Running docker stack deploy")

				// Run docker stack deploy
				args := []string{"stack", "deploy", "--with-registry-auth"}
				if prune {
					args = append(args, "--prune")
				}
				for _, composeFile := range composeFiles {
					args = append(args, "--compose-file", composeFile)
				}
				args = append(args, namespace)
				cmd := exec.Command("docker", args...)
				if output, err := cmd.CombinedOutput(); err != nil {
					log.WithFields(logrus.Fields{
						"output": string(output),
						"error":  err,
					}).Fatal("Error during docker stack deploy")
				} else if cmd.ProcessState.Success() {
					log.WithFields(logrus.Fields{
						"output": string(output),
					}).Debug("docker stack deploy command completed")
				} else {
					log.WithFields(logrus.Fields{
						"output": string(output),
					}).Fatal("docker stack deploy command failed")
				}
			}

			// Monitor for end state
			checkTicker := time.NewTicker(checkInterval)
			defer checkTicker.Stop()
			statusTicker := time.NewTicker(statusInterval)
			defer statusTicker.Stop()
			mux := &sync.RWMutex{}
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()
			statusContext := statusContextType{ctx, mux, updating, completed, untouched, rolledBack, rollbackStarted, failed, successfulTasks, desiredReplicas, servicesBefore, serviceNames, nodeIDs}
			sigChan := make(chan os.Signal, 1)
			signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM)
			check(statusContext)
			for done := false; !done; {
				select {
				case <-statusTicker.C:
					printPrettyReport(statusContext)
					printDebugReport(statusContext)
				case <-checkTicker.C:
					done = check(statusContext)
				case s := <-sigChan:
					return fmt.Errorf("Aborted by %v", s.String())
				}
			}
			untouchedKeys := []string{}
			for _, name := range serviceNames {
				_, inCompleted := completed[name]
				_, inRolledBack := rolledBack[name]
				_, inFailed := failed[name]
				if !inCompleted && !inRolledBack && !inFailed {
					untouchedKeys = append(untouchedKeys, name)
				}
			}
			rolledBackKeys := make([]string, 0, len(rolledBack))
			for name, message := range rolledBack {
				rolledBackKeys = append(rolledBackKeys, fmt.Sprintf("%s (%s)", name, message))
			}
			if len(rolledBack) > 0 {
				log.WithFields(logrus.Fields{
					"services":  rolledBackKeys,
					"untouched": untouchedKeys,
				}).Fatal("Some services were rolled back")
			}
			failedKeys := make([]string, 0, len(failed))
			for name, failedStatus := range failed {
				failedKeys = append(failedKeys, fmt.Sprintf("%s (%s)", name, failedStatus.Err))
			}
			if len(failed) > 0 {
				log.WithFields(logrus.Fields{
					"services":  failedKeys,
					"untouched": untouchedKeys,
				}).Fatal("Some services failed")
			}
			completedKeys := make([]string, 0, len(completed))
			for name := range completed {
				completedKeys = append(completedKeys, name)
			}
			if prettyReport {
				printPrettyReport(statusContext)
			}
			log.WithFields(logrus.Fields{
				"services":  completedKeys,
				"untouched": untouchedKeys,
			}).Info("All services deployed")

			return nil
		},
	}
	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

func printPrettyReport(ctx statusContextType) {
	ctx.mu.RLock()
	defer ctx.mu.RUnlock()
	// initialize tabwriter
	w := new(tabwriter.Writer)

	// minwidth, tabwidth, padding, padchar, flags
	w.Init(os.Stdout, 7, 8, 1, ' ', tabwriter.TabIndent)

	defer w.Flush()

	if len(ctx.rolledBack) > 0 || len(ctx.rollbackStarted) > 0 {
		fmt.Fprintf(w, "\n %s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t", "Name", "Updating", "Completed", "Failed", "Untouched", "Successful tasks", "Rolled back", "Rollback started")
		fmt.Fprintf(w, "\n %s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t", "----", "----", "----", "----", "----", "----", "----", "----")
	} else {
		fmt.Fprintf(w, "\n %s\t%s\t%s\t%s\t%s\t%s\t", "Name", "Updating", "Completed", "Failed", "Untouched", "Successful tasks")
		fmt.Fprintf(w, "\n %s\t%s\t%s\t%s\t%s\t%s\t", "----", "----", "----", "----", "----", "----")
	}
	sort.Strings(ctx.serviceNames)
	for _, k := range ctx.serviceNames {
		updating := " "
		completed := " "
		failed := " "
		untouched := " "
		rolledBack := " "
		rollbackStarted := " "
		successfulTasks := " "
		if _, exists := ctx.updating[k]; exists {
			updating = "x"
		}
		if _, exists := ctx.completed[k]; exists {
			completed = "x"
		}
		if _, exists := ctx.untouched[k]; exists {
			untouched = "x"
		}
		if failedStatus, exists := ctx.failed[k]; exists {
			failed = fmt.Sprintf("%d", failedStatus.Count)
		}
		if numSuccessfulTasks, exists := ctx.successfulTasks[k]; exists {
			if desiredReplicas, exists := ctx.desiredReplicas[k]; exists {
				successfulTasks = fmt.Sprintf("%d/%d", numSuccessfulTasks, desiredReplicas)
			} else {
				successfulTasks = fmt.Sprintf("%d", numSuccessfulTasks)
			}
		}
		if len(ctx.rolledBack) > 0 || len(ctx.rollbackStarted) > 0 {
			if _, exists := ctx.rolledBack[k]; exists {
				rolledBack = "x"
			}
			if _, exists := ctx.rollbackStarted[k]; exists {
				rollbackStarted = "x"
			}
			fmt.Fprintf(w, "\n %s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t", k, updating, completed, failed, untouched, rolledBack, rollbackStarted, successfulTasks)
		} else {
			fmt.Fprintf(w, "\n %s\t%s\t%s\t%s\t%s\t%s\t", k, updating, completed, failed, untouched, successfulTasks)
		}
	}
	fmt.Fprintln(w)
	w.Flush()
}

func printDebugReport(ctx statusContextType) {
	if debug {
		ctx.mu.RLock()
		defer ctx.mu.RUnlock()
		log.WithFields(logrus.Fields{
			"updating":         ctx.updating,
			"completed":        ctx.completed,
			"rolledBack":       ctx.rolledBack,
			"rollbackStarted":  ctx.rollbackStarted,
			"failed":           ctx.failed,
			"successful tasks": ctx.successfulTasks,
			"desired replicas": ctx.desiredReplicas,
		}).Debug("Status")
	}
}

func check(ctx statusContextType) bool {
	ctx.mu.Lock()
	defer ctx.mu.Unlock()
	for _, name := range ctx.serviceNames {
		_, isUntouched := ctx.untouched[name]
		// Inspect service to get current spec
		service := mustInspectService(name)
		if _, exists := ctx.updating[name]; !isUntouched && !exists {
			ctx.updating[name] = struct{}{}
			log.WithFields(logrus.Fields{
				"service": name,
			}).Info("Service create started")
		}
		taskFilters := filters.NewArgs(filters.Arg("name", fmt.Sprintf("%s.", name)))
		for _, nodeID := range ctx.nodeIDs {
			taskFilters.Add("node", nodeID)
		}
		currentTasks, err := dockerCLI.TaskList(context.Background(), types.TaskListOptions{
			Filters: taskFilters,
		})
		if err != nil {
			log.WithFields(logrus.Fields{
				"error":   err,
				"service": name,
			}).Error("Error getting tasks for service")
		}

		// Prepare for counting running/completed/any tasks
		var successTaskStates []swarm.TaskState
		tasksAllowedToFail := false
		if service.Spec.TaskTemplate.RestartPolicy != nil {
			switch service.Spec.TaskTemplate.RestartPolicy.Condition {
			case swarm.RestartPolicyConditionOnFailure:
				successTaskStates = []swarm.TaskState{swarm.TaskStateRunning, swarm.TaskStateComplete}
			case swarm.RestartPolicyConditionNone:
				tasksAllowedToFail = true
			default:
				successTaskStates = []swarm.TaskState{swarm.TaskStateRunning}
			}
		}

		taskStates := map[swarm.TaskState]uint64{}
		var taskVersionIndexThreshold uint64
		serviceBefore, serviceExistedBeforeDeploy := ctx.servicesBefore[name]
		if serviceExistedBeforeDeploy {
			// Service was potentially updated by our deploy; compare previous and current spec
			taskTemplateBefore := serviceBefore.Spec.TaskTemplate
			taskTemplateNow := service.Spec.TaskTemplate
			if !reflect.DeepEqual(taskTemplateBefore, taskTemplateNow) {
				// Spec differences found; only count newer tasks, which should be different from current tasks
				taskVersionIndexThreshold = serviceBefore.Version.Index
			} else {
				// Service specs identical; count _all_ tasks as there may be existing tasks that satisfy the desired state
				if _, exists := ctx.untouched[name]; !exists {
					ctx.untouched[name] = struct{}{}
					delete(ctx.updating, name)
					log.WithFields(logrus.Fields{
						"service": name,
					}).Info("Task template for service identical; will count all running tasks as valid")
				}
				taskVersionIndexThreshold = 0
			}
		}
		// Collect the number of tasks in each state desired to be running
		tasksDesiredToBeRunning := []swarm.Task{}
		for _, task := range currentTasks {
			state := task.Status.State
			taskStates[state]++
			// task.Version.Index is always greater than 0
			if task.Version.Index > taskVersionIndexThreshold && task.DesiredState == swarm.TaskStateRunning {
				tasksDesiredToBeRunning = append(tasksDesiredToBeRunning, task)
			}
		}

		// Check if the service satisfies the desired state
		var successfulTasks uint64
		for _, successTaskState := range successTaskStates {
			successfulTasks += taskStates[successTaskState]
		}
		ctx.successfulTasks[name] = successfulTasks
		if service.Spec.Mode.Replicated != nil && service.Spec.Mode.Replicated.Replicas != nil {
			ctx.desiredReplicas[name] = *service.Spec.Mode.Replicated.Replicas
		}

		if tasksAllowedToFail || service.Spec.Mode.Global != nil || successfulTasks == *service.Spec.Mode.Replicated.Replicas {
			if _, exists := ctx.completed[name]; !exists {
				ctx.completed[name] = struct{}{}
				delete(ctx.failed, name)
				log.WithFields(logrus.Fields{
					"service": name,
				}).Info("Service create completed")
				continue
			}
		}

		// Determine if we need to increase the number of times the service has had failed tasks
		for _, task := range tasksDesiredToBeRunning {
			state := task.Status.State
			switch state {
			case swarm.TaskStateFailed:
				currentFailedStatus, exists := ctx.failed[name]
				count := 1
				if exists {
					count = currentFailedStatus.Count + 1
				}
				ctx.failed[name] = failedStatus{
					Err:   "need to check the tasks for this one",
					Count: count,
				}
				if count == 1 {
					log.WithFields(logrus.Fields{
						"service":   name,
						"error":     task.Status.Err,
						"message":   task.Status.Message,
						"container": task.Status.ContainerStatus.ContainerID,
					}).Warn("Service task failed")
				}
				break
			default:
				_, isUntouched := ctx.untouched[name]
				if _, exists := ctx.updating[name]; !isUntouched && !exists {
					ctx.updating[name] = struct{}{}
					log.WithFields(logrus.Fields{
						"service":            name,
						"task.desired_state": task.DesiredState,
						"task.state":         task.Status.State,
						"task.message":       task.Status.Message,
					}).Info("Service task not running or failed; putting in \"updating\" state")
				}
			}
		}
	}

	// Count how many services have crossed the threshold for failed tasks and are thus considered failed entirely.
	// Services that turned out to be fine will already have been removed from the "failed" map at this point
	failedAboveThreshold := 0
	for _, failedStatus := range ctx.failed {
		if failedStatus.Count > failCountThreshold {
			failedAboveThreshold++
		}
	}
	if len(ctx.updating)+len(ctx.untouched) == len(ctx.completed)+len(ctx.rolledBack)+failedAboveThreshold {
		return true
	}
	return false
}

func mustInspectService(name string) *swarm.Service {
	service, _, err := dockerCLI.ServiceInspectWithRaw(context.Background(), name, types.ServiceInspectOptions{
		InsertDefaults: true,
	})
	if err != nil {
		log.WithFields(logrus.Fields{
			"service": name,
			"error":   err,
		}).Fatal("Error inspecting service")
	}
	return &service
}

func inspectService(name string) *swarm.Service {
	service, _, _ := dockerCLI.ServiceInspectWithRaw(context.Background(), name, types.ServiceInspectOptions{
		InsertDefaults: true,
	})
	return &service
}
