FROM golang:1.17.0 AS base
ARG MODULE=gitlab.com/almbrand/docker-stack-waiter
ENV GO111MODULE=on
WORKDIR /go/src/$MODULE
COPY go.mod go.sum ./
RUN go mod download
COPY . .
ARG version=dev
RUN CGO_ENABLED=0 GOOS=linux go install -v -a -installsuffix cgo -ldflags "-w -X main.version=${version}" .

FROM golang:1.17.0 AS revive
RUN go get -v github.com/mgechev/revive

FROM base AS lint
COPY --from=revive /go/bin/revive /go/bin/revive
RUN revive --formatter friendly ./... $(go list ./...)

FROM base AS vet
RUN go vet -v

FROM base AS test
RUN go test -short $(go list ./...)

FROM base AS race
RUN go test -race -short $(go list ./...)

FROM scratch
COPY --from=base /go/bin/docker-stack-waiter /docker-stack-waiter
ENTRYPOINT ["/docker-stack-waiter"]
